# Attitude estimation of smartphone

## Project purpose

Attitude estimation is an integral part of many technical systems in today's and plays an important role in everything from mobile game applications to advanced control systems. This report aims to cover the topic of orientation estimation of a Android smartphone using some of its key sensors namely the accelerometer, gyroscope and magnetometer. 

The estimation will be done by constructing a model of the smartphone and then fusing the measurements from these sensors using an Extenden Kalman Filter (EKF). The orientation of the phone will be modeled using the quaternion representation. This way of modeling the system is chosen since this approach suffers less from common issues encountered when using rotation matrices (not being a minimal representation) and Euler angles (such as the gimbal lock effect).