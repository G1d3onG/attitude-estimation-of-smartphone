function [] = plot_measurements(data,t,type)
    figure('Position',[300 300 600 400]);
    plot(t, data)
    xlabel('t')
    ylabel('y(t)')
    legend(append(type,' x'),append(type,' y'),append(type,' z'))
    title(append(type,' measurements'))
    print(append('Figs/2_',type,'.eps'),'-depsc');
end

