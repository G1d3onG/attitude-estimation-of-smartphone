function [] = plot_measurement_histogram(data,mu,sigma,type)
    figure('Position',[300 300 600 400]);
    ax = linspace(mu-3*sqrt(sigma), mu+3*sqrt(sigma),1000);
    histogram(data(1,:),20,'Normalization','pdf')
    hold on
    plot(ax,normpdf(ax,mu,sqrt(sigma)),'LineWidth',3,'color','black')
    xlim([mu-5*sqrt(sigma), mu+5*sqrt(sigma)])
    legend('Histogram of samples', 'Estimated Gaussian distribution')
    title(append('Histogram over ',type,' measurements'))
    print(append('Figs/2_histo_',type,'.eps'),'-depsc');
end

