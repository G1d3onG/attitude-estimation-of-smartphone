function [x,P] = mu_m(x,P,mag,m0,Rm)
    h = Qq(x).'*m0;
    [Q0, Q1, Q2, Q3] = dQqdq(x);
    h_prim = [Q0.'*m0 Q1.'*m0 Q2.'*m0 Q3.'*m0];
    if any(isnan(mag))
        mag = h;
    end
    Sk = h_prim*P*h_prim.' + Rm;
    Kk = P*h_prim.'*inv(Sk);
    x = x + Kk*(mag - h); 
    P = P - Kk*Sk*Kk.';
    [x,P] = mu_normalizeQ(x,P);
end

