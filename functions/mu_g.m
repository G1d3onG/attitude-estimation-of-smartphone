function [x,P] = mu_g(x,P,yacc,Ra,g0)
    h = Qq(x).'*g0;
    [Q0, Q1, Q2, Q3] = dQqdq(x);
    h_prim = [Q0.'*g0 Q1.'*g0 Q2.'*g0 Q3.'*g0];
    if any(isnan(yacc))
        yacc = h;
    end
    Sk = h_prim*P*h_prim.' + Ra;
    Kk = P*h_prim.'*inv(Sk);
    x = x + Kk*(yacc - h); 
    P = P - Kk*Sk*Kk.';
    [x,P] = mu_normalizeQ(x,P);
end

