function [gyro,t_gyr,acc,t_acc,mag,t_mag] = clean_measurement_data(meas)
    N=size(meas.gyr,2);
    gyro = [];
    acc = [];
    mag = [];   
    t_gyr = [];
    t_acc = [];
    t_mag = [];
    
    for i=1:N
        if ~isnan(meas.gyr(:,i))&i>200
            gyro=[gyro meas.gyr(:,i)];
            t_gyr=[t_gyr meas.t(i)];
        end
    
        if ~isnan(meas.acc(:,i))&i>200
            acc=[acc meas.acc(:,i)];
            t_acc=[t_acc meas.t(i)];
        end
    
        if ~isnan(meas.mag(:,i))&i>200
            mag=[mag meas.mag(:,i)];
            t_mag=[t_mag meas.t(i)];
        end
    
    end
end

