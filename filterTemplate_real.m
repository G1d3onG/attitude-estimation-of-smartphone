function [xhat, meas] = filterTemplate_real(calAcc, calGyr, calMag)
% FILTERTEMPLATE  Filter template
%
% This is a template function for how to collect and filter data
% sent from a smartphone live.  Calibration data for the
% accelerometer, gyroscope and magnetometer assumed available as
% structs with fields m (mean) and R (variance).
%
% The function returns xhat as an array of structs comprising t
% (timestamp), x (state), and P (state covariance) for each
% timestamp, and meas an array of structs comprising t (timestamp),
% acc (accelerometer measurements), gyr (gyroscope measurements),
% mag (magnetometer measurements), and orint (orientation quaternions
% from the phone).  Measurements not availabe are marked with NaNs.
%
% As you implement your own orientation estimate, it will be
% visualized in a simple illustration.  If the orientation estimate
% is checked in the Sensor Fusion app, it will be displayed in a
% separate view.
%
% Note that it is not necessary to provide inputs (calAcc, calGyr, calMag).

  %% Setup necessary infrastructure
  import('com.liu.sensordata.*');  % Used to receive data.

  %% Filter settings
  t0 = [];  % Initial time (initialize on first data received)
  nx = 4;   % Assuming that you use q as state variable.
  % Add your filter settings here.

  % Current filter state.
  x = [1; 0; 0 ;0];
  P = eye(nx, nx);
  %mu_m=[14.1394230769231	;41.3830416955417	;-56.9163923538924];
  %mum=[-3.78547297297297;	6.00126689189189	;-42.1417294698545];
%   m0=[0; 
%       sqrt(mum(1)^2+mum(2)^2); 
%       mum(3)];
  Lk=30;
  g0=[0.0129516593788878;	
      0.0794879376039431;
      9.87807416476007];

  sigma_g = 0.05*[3.12193922585303e-07	-9.24815118741765e-08	5.90637731919007e-09;
    -9.24815118741765e-08	4.33855039049033e-07	2.81029307630614e-09;
    5.90637731919007e-09	2.81029307630614e-09	3.30809128100271e-07;];

  sigma_a=[4.594298355350883e-05,-4.241336180020069e-06,4.807135372693124e-05;
      -4.241336180020069e-06,4.389296677878661e-05,-1.379199969758925e-05;
      4.807135372693124e-05,-1.379199969758925e-05,0.001086159243943];
%   sigma_m=[0.128384990846723	,-0.00894569018377648,	-0.00943383257944690;
% -0.00894569018377648	,0.252246754788345	,0.130889961314667;
% -0.00943383257944690	,0.130889961314667,	0.293972614680433];
sigma_m=0.01*[0.150201134734579	,-0.00252060758106000	,-0.00390888524054476;
-0.00252060758106000	,0.174126140510147,	0.00429918907060337;
-0.00390888524054476	,0.00429918907060337,	0.158702119833104];

  % Saved filter states.
  xhat = struct('t', zeros(1, 0),...
                'x', zeros(nx, 0),...
                'P', zeros(nx, nx, 0));

  meas = struct('t', zeros(1, 0),...
                'acc', zeros(3, 0),...
                'gyr', zeros(3, 0),...
                'mag', zeros(3, 0),...
                'orient', zeros(4, 0));
  try
    %% Create data link
    server = StreamSensorDataReader(3400);
    % Makes sure to resources are returned.
    sentinel = onCleanup(@() server.stop());

    server.start();  % Start data reception.

    % Used for visualization.
    figure(1);
    subplot(1, 2, 1);
    ownView = OrientationView('Own filter', gca);  % Used for visualization.
    googleView = [];
    counter = 0;  % Used to throttle the displayed frame rate.
    
    i = 0;
    mag_cal = [];
    m0 = 0;
    run_mag = false;
%     mum = mean(mag_cal);
%     m0=[0; 
%       sqrt(mum(1)^2+mum(2)^2); 
%       mum(3)];

    %% Filter loop
    while server.status()  % Repeat while data is available
      % Get the next measurement set, assume all measurements
      % within the next 5 ms are concurrent (suitable for sampling
      % in 100Hz).
      data = server.getNext(5);

      if i<200 && ~any(isnan(data(1, 8:10)))
          mag_cal = [mag_cal data(1, 8:10)'];
          if i > 1
              mum = mean(mag_cal);
              m0=[0; 
                  sqrt(mum(1)^2+mum(2)^2); 
                  mum(3)];
              run_mag = true;
          end
         i = i+1;
      end  
      %mum=[-3.78547297297297;	6.00126689189189	;-42.1417294698545];

      if isnan(data(1))  % No new data received
        continue;        % Skips the rest of the look
      end
      t = data(1)/1000;  % Extract current time

      if isempty(t0)  % Initialize t0
        t0 = t;
      end
    
      acc = data(1, 2:4)';
      yacc=acc; 
      reject = false;
      if ~any(isnan(acc))  % Acc measurements are available.
        if abs(norm(yacc)/norm(g0)) < 0.9 || abs(norm(yacc)/norm(g0)) > 1.1
            ownView.setAccDist(1)
            reject = true;
        else
            ownView.setAccDist(0)
        end
      end

      gyr = data(1, 5:7)';
      omega = zeros(3,1); 
      T = 0.01;
      if ~any(isnan(gyr))  % Gyro measurements are available.
        omega=gyr;
        T=t-t0-meas.t(end);
      end
      
      [x, P] = tu_qw(x, P,omega, T, sigma_g);
      if ~reject
         [x,P] = mu_g(x,P,yacc,sigma_a,g0);
      end
      
      
      alpha=0.1;
      reject_mag=false;
      mag = data(1, 8:10)';

      if ~any(isnan(mag))  % Mag measurements are available.
        Lk=(1-alpha)*Lk+alpha*norm(mag);
        if Lk < 30 || Lk > 55
            ownView.setMagDist(1)
            reject_mag = true;
        else
            ownView.setMagDist(0)
        end
      end

      if ~reject_mag && run_mag
          [x,P] = mu_m(x,P,mag,m0,sigma_m); % we get some error using the same but with other name
      end
      orientation = data(1, 18:21)';  % Google's orientation estimate.

      % Visualize result
      if rem(counter, 10) == 0
        setOrientation(ownView, x(1:4));
        title(ownView, 'OWN', 'FontSize', 16);
        if ~any(isnan(orientation))
          if isempty(googleView)
            subplot(1, 2, 2);
            % Used for visualization.
            googleView = OrientationView('Google filter', gca);
          end
          setOrientation(googleView, orientation);
          title(googleView, 'GOOGLE', 'FontSize', 16);
        end
      end
      counter = counter + 1;

      % Save estimates
      xhat.x(:, end+1) = x;
      xhat.P(:, :, end+1) = P;
      xhat.t(end+1) = t - t0;

      meas.t(end+1) = t - t0;
      meas.acc(:, end+1) = acc;
      meas.gyr(:, end+1) = gyr;
      meas.mag(:, end+1) = mag;
      meas.orient(:, end+1) = orientation;
    end
  catch e
    fprintf(['Unsuccessful connecting to client!\n' ...
      'Make sure to start streaming from the phone *after*'...
             'running this function!']);
  end
end
